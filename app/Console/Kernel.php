<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Http\Request;

use App\Models\Email;
use App\Models\Inbox;
use App\Models\Spam;
use App\Models\Sent;
use App\Models\Drafts;
use App\Models\Trash;
use App\Models\Junk;
use App\Models\Attachments;
use App\Models\Errorlogs;

use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;


class Kernel extends ConsoleKernel
{

    protected $commands = [
        //
    ];

    public function saveMail($result,$mailboxType,$mailbox,$stream,$j,$accountId,$appUrl)
    {

        $i=0;
        while(1)
        {
            if($i>=100)
                break;
            try
            {
                if(isset($result[$i]))    
                {
                    $mail=$mailbox->getMail($result[$i],false);

                    $status = imap_headerinfo($stream,$j);
                    $status = ( $status->Unseen == 'U') ? 'unread' : 'read';

                    if($mailboxType=="Inbox")
                        $mbVar = new Inbox;
                    elseif($mailboxType=="Sent")
                        $mbVar = new Sent;
                    elseif($mailboxType=="Spam")
                        $mbVar = new Spam;
                    elseif($mailboxType=="Drafts")
                        $mbVar = new Drafts;
                    elseif($mailboxType=="Trash")
                        $mbVar = new Trash;
                    elseif($mailboxType=="Junk")
                        $mbVar = new Junk;
                         
                    $mbVar->accountId = $accountId;
                    $mbVar->flag = 'NULL';
                    $mbVar->mailId = $mail->id;
                    $mbVar->date = $mail->date;
                    $mbVar->subject = $mail->subject;
                    $mbVar->fromName = $mail->fromName;
                    $mbVar->fromAddress = $mail->fromAddress;
                    $mbVar->to = implode(",",$mail->to);
                    $mbVar->cc = implode(",",$mail->cc);
                    $mbVar->replyTo = implode(",",$mail->replyTo);
                    $mbVar->textPlain = $mail->textPlain;
                    $mbVar->textHtml = $mail->textHtml;
                    $mbVar->status = $status;
                    try
                    {
                        $mbVar->save();
                    }
                    catch(\Exception $errmsg)
                    {
                        $mbVar->textPlain = $mbVar->textHtml = "error";
                        $mbVar->save();
                    }

                    $attachments = $mail->getAttachments();

                    if($attachments!=NULL)
                    {
                        foreach($attachments as $attachmentData)
                        {

                            if(!is_numeric($attachmentData->id))
                                break;

                            $attachment = new Attachments;
                            $attachment->mailId = $mbVar->id;
                            $attachment->mailboxType = $mailboxType;
                            $attachment->attachmentId = $attachmentData->id;
                            $attachment->fileName = $attachmentData->name;
                            $attachment->filePath = $appUrl."/storage/mail-attachments/".basename($attachmentData->filePath);

                            $attachment->save();
                        }

                    }

                    $i++;
                }
                else
                    break;
            }
            catch(\Exception $err)
            {
                $errorLog = new Errorlogs;
                $errorLog->errormsg = $err->getMessage();
                $errorLog->save();
                $i++;
            }
        }

    }
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function (Request $request) 
        {
            $i=0;

            $mailBox=Email::get();
            foreach ($mailBox as $data) 
            {
                try
                {
                    $mbox = imap_open("{saniservice.com:993/imap/ssl}", $data->email,$data->password, OP_HALFOPEN)
                    or die("can't connect: " . imap_last_error());

                    $list = imap_getmailboxes($mbox, "{saniservice.com:993/imap/ssl}", "*");

                    $j=1;
                    if(is_array($list)) 
                    {
                        foreach ($list as $key => $val) 
                        {
                            $mailbox = new ImapMailbox($val->name,$data->email,$data->password,storage_path().'/mail-attachments');
                            $mailIds = $mailbox->searchMailbox('ALL');
                            $stream = imap_open($val->name, $data->email,$data->password);
                            if($val->name=="{saniservice.com:993/imap/ssl}INBOX")
                            {
                                $dbmailIds=Inbox::where('accountId',$data->id)->pluck('mailId')->toArray();
                                $result=array_values(array_diff($mailIds,$dbmailIds));

                                $this->saveMail($result,'Inbox',$mailbox,$stream,$j,$data->id,$request->url());

                            }
                            elseif ($val->name=="{saniservice.com:993/imap/ssl}INBOX.spam") 
                            {
                                $dbmailIds=Spam::where('accountId',$data->id)->pluck('mailId')->toArray();
                                $result=array_values(array_diff($mailIds,$dbmailIds));

                                $this->saveMail($result,'Spam',$mailbox,$stream,$j,$data->id,$request->url());

                            }
                            elseif ($val->name=="{saniservice.com:993/imap/ssl}INBOX.Drafts") 
                            {
                                $dbmailIds=Drafts::where('accountId',$data->id)->pluck('mailId')->toArray();
                                $result=array_values(array_diff($mailIds,$dbmailIds));

                                $this->saveMail($result,'Drafts',$mailbox,$stream,$j,$data->id,$request->url());

                            }
                            elseif ($val->name=="{saniservice.com:993/imap/ssl}INBOX.Sent") 
                            {
                                $dbmailIds=Sent::where('accountId',$data->id)->pluck('mailId')->toArray();
                                $result=array_values(array_diff($mailIds,$dbmailIds));

                                $this->saveMail($result,'Sent',$mailbox,$stream,$j,$data->id,$request->url());

                            }
                            elseif ($val->name=="{saniservice.com:993/imap/ssl}INBOX.Trash") 
                            {
                                $dbmailIds=Trash::where('accountId',$data->id)->pluck('mailId')->toArray();
                                $result=array_values(array_diff($mailIds,$dbmailIds));

                                $this->saveMail($result,'Trash',$mailbox,$stream,$j,$data->id,$request->url());

                            }
                            elseif($val->name=="{saniservice.com:993/imap/ssl}INBOX.Junk")
                            {                                
                                $dbmailIds=Junk::where('accountId',$data->id)->pluck('mailId')->toArray();
                                $result=array_values(array_diff($mailIds,$dbmailIds));
                                $this->saveMail($result,'Junk',$mailbox,$stream,$j,$data->id,$request->url());

                            }
                        }

                    }
                    imap_close($mbox);
                }
                catch(\Exception $e)
                {
                    $errorLog = new Errorlogs;
                    $errorLog->errormsg = $e->getMessage();
                    $errorLog->save();
                }
            }
            


        })->everyMinute()->name('mailfetch')->withoutOverlapping();
    }


    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
