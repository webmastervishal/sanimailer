<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Inbox;
use App\Models\Sent;
use App\Models\Junk;
use App\Models\Spam;
use App\Models\Drafts;
use App\Models\Trash;

use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;


class MailBoxController extends Controller
{
    public function getInbox(Request $request)
    {
    	$mails=Inbox::where('accountId',$request->id)->orderBy('date','desc')->with('attachment')->paginate(50);
    	return $mails;
    }

    public function getSent(Request $request)
    {
    	$mails=Sent::where('accountId',$request->id)->orderBy('date','desc')->with('attachment')->paginate(50);
    	return $mails;
    }

    public function getJunk(Request $request)
    {
    	$mails=Junk::where('accountId',$request->id)->orderBy('date','desc')->with('attachment')->paginate(50);
    	return $mails;
    }

    public function getSpam(Request $request)
    {
    	$mails=Spam::where('accountId',$request->id)->orderBy('date','desc')->with('attachment')->paginate(50);
    	return $mails;
    }

    public function getDrafts(Request $request)
    {
    	$mails=Drafts::where('accountId',$request->id)->orderBy('date','desc')->with('attachment')->paginate(50);
    	return $mails;
    }

    public function getTrash(Request $request)
    {
    	$mails=Trash::where('accountId',$request->id)->orderBy('date','desc')->with('attachment')->paginate(100);
    	return $mails;
    }

    public function getInboxMail(Request $request)
    {
 
        $mail=Inbox::where('id',$request->id)->with('attachment')->first();

    	return $mail;
    }


    public function getSentMail(Request $request)
    {
    	$mail=Sent::where('id',$request->id)->with('attachment')->first();
    	return $mail;
    }


    public function getJunkMail(Request $request)
    {
    	$mail=Junk::where('id',$request->id)->with('attachment')->first();
    	return $mail;
    }


    public function getSpamMail(Request $request)
    {
    	$mail=Spam::where('id',$request->id)->with('attachment')->first();
    	return $mail;
    }


    public function getDraftsMail(Request $request)
    {
    	$mail=Drafts::where('id',$request->id)->with('attachment')->first();
    	return $mail;
    }

    public function getTrashMail(Request $request)
    {
    	$mail=Trash::where('id',$request->id)->with('attachment')->first();
    	return $mail;
    }


    public function getInboxCount(Request $request)
    {
        $count = Inbox::where('status','unread')->count();
        return $count;
    }

    public function getSentCount(Request $request)
    {
        $count = Sent::where('status','unread')->count();
        return $count;
    }

    public function getSpamCount(Request $request)
    {
        $count = Spam::where('status','unread')->count();
        return $count;
    }


    public function getTrashCount(Request $request)
    {
        $count = Trash::where('status','unread')->count();
        return $count;
    }


    public function getDraftsCount(Request $request)
    {
        $count = Drafts::where('status','unread')->count();
        return $count;
    }


    public function getJunkCount(Request $request)
    {
        $count = Junk::where('status','unread')->count();
        return $count;
    }

    public function setInboxStatus($id)
    {
        Inbox::where('id',$id)->update(['status'=>'read']);
        $mailId = Inbox::with('emailAccount')->where('id',$id)->first();

        $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX", $mailId->emailAccount->email,$mailId->emailAccount->password);
        $status = imap_setflag_full($mbox,$mailId->mailId, "\Seen",ST_UID); 
        return "true";

    }


    public function setSentStatus($id)
    {
        Sent::where('id',$id)->update(['status'=>'read']);
        $mailId = Sent::with('emailAccount')->where('id',$id)->first();

        $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Sent", $mailId->emailAccount->email,$mailId->emailAccount->password);
        $status = imap_setflag_full($mbox,$mailId->mailId, "\Seen",ST_UID); 
                    

        return "true";

    }

    public function setSpamStatus($id)
    {
        Spam::where('id',$id)->update(['status'=>'read']);
        $mailId = Spam::with('emailAccount')->where('id',$id)->first();

        $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.spam", $mailId->emailAccount->email,$mailId->emailAccount->password);
        $status = imap_setflag_full($mbox,$mailId->mailId, "\Seen",ST_UID);              

        return "true";

    }

    public function setJunkStatus($id)
    {
        Junk::where('id',$id)->update(['status'=>'read']);
        $mailId = Junk::with('emailAccount')->where('id',$id)->first();

        $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Junk", $mailId->emailAccount->email,$mailId->emailAccount->password);
        $status = imap_setflag_full($mbox,$mailId->mailId, "\Seen",ST_UID);             

        return "true";

    }

    public function setDraftsStatus($id)
    {
        Drafts::where('id',$id)->update(['status'=>'read']);
        $mailId = Drafts::with('emailAccount')->where('id',$id)->first();

        $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Drafts", $mailId->emailAccount->email,$mailId->emailAccount->password);
        $status = imap_setflag_full($mbox,$mailId->mailId, "\Seen",ST_UID);              

        return "true";

    }

    public function setTrashStatus($id)
    {
        Trash::where('id',$id)->update(['status'=>'read']);
        $mailId = Trash::with('emailAccount')->where('id',$id)->first();

        $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Trash", $mailId->emailAccount->email,$mailId->emailAccount->password);
        $status = imap_setflag_full($mbox,$mailId->mailId, "\Seen",ST_UID);             

        return "true";

    }

    public function deleteInboxMail($mailIds)
    {
        $mailIds = explode(',',$mailIds);
        foreach($mailIds as $id)
        {
            $mail=Inbox::with('emailAccount')->where('id',$id)->first()->toArray();
            $email=$mail['email_account']['email'];
            $password=$mail['email_account']['password'];
            unset($mail['id']);
            unset($mail['email_account']);
            Trash::insert($mail);
            Inbox::where('id',$id)->delete();

            $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX", $email,$password);
            imap_mail_move($mbox,$mail['mailId'], 'INBOX.Trash',CP_UID);
            imap_expunge($mbox);

        }
        
    }


    public function deleteSpamMail($mailIds)
    {
        $mailIds = explode(',',$mailIds);
        foreach($mailIds as $id)
        {
            $mail=Spam::with('emailAccount')->where('id',$id)->first()->toArray();
            $email=$mail['email_account']['email'];
            $password=$mail['email_account']['password'];
            unset($mail['id']);
            unset($mail['email_account']);
            Trash::insert($mail);
            Spam::where('id',$id)->delete();

            $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.spam", $email,$password);
            imap_mail_move($mbox,$mail['mailId'], 'INBOX.Trash',CP_UID);
            imap_expunge($mbox);
        }
    }


    public function deleteSentMail($mailIds)
    {
        $mailIds = explode(',',$mailIds);
        foreach($mailIds as $id)
        {

            $mail=Sent::with('emailAccount')->where('id',$id)->first()->toArray();
            $email=$mail['email_account']['email'];
            $password=$mail['email_account']['password'];
            unset($mail['id']);
            unset($mail['email_account']);
            Trash::insert($mail);
            Sent::where('id',$id)->delete();

            $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Sent", $email,$password);
            imap_mail_move($mbox,$mail['mailId'], 'INBOX.Trash',CP_UID);
            imap_expunge($mbox);
        }
        
    }


    public function deleteJunkMail($mailIds)
    {
        $mailIds = explode(',',$mailIds);
        foreach($mailIds as $id)
        {
            $mail=Junk::with('emailAccount')->where('id',$id)->first()->toArray();
            $email=$mail['email_account']['email'];
            $password=$mail['email_account']['password'];
            unset($mail['id']);
            unset($mail['email_account']);
            Trash::insert($mail);
            Junk::where('id',$id)->delete();

            $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Junk", $email,$password);
            imap_mail_move($mbox,$mail['mailId'], 'INBOX.Trash',CP_UID);
            imap_expunge($mbox);


        }
        
    }


    public function deleteDraftsMail($mailIds)
    {
        $mailIds = explode(',',$mailIds);
        foreach($mailIds as $id)
        {
            $mail=Drafts::with('emailAccount')->where('id',$id)->first()->toArray();
            $email=$mail['email_account']['email'];
            $password=$mail['email_account']['password'];
            unset($mail['id']);
            unset($mail['email_account']);
            Trash::insert($mail);
            Drafts::where('id',$id)->delete();

            $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Drafts", $email,$password);
            imap_mail_move($mbox,$mail['mailId'], 'INBOX.Trash',CP_UID);
            imap_expunge($mbox);



        }
        
    }

    public function deleteTrashMail($mailIds)
    {
        $mailIds = explode(',',$mailIds);
        foreach($mailIds as $id)
        {
            $mail=Trash::with('emailAccount')->where('id',$id)->first()->toArray();
            $email=$mail['email_account']['email'];
            $password=$mail['email_account']['password'];

            Trash::where('id',$id)->delete();

            $mbox = imap_open("{saniservice.com:993/imap/ssl}INBOX.Trash", $email,$password);
            imap_delete($mbox,$mail['mailId'],FT_UID);
            imap_expunge($mbox);
        }
        
    }

}
