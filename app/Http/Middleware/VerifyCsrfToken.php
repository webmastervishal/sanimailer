<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    	'set-inbox-status/*',
    	'set-sent-status/*',
    	'set-spam-status/*',
    	'set-drafts-status/*',
    	'set-trash-status/*',
    	'set-junk-status/*',
    ];
}
