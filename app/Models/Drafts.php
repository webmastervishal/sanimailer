<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Drafts extends Model
{
    public $table="drafts";
    public $fillable=['accountId','flag','mailId','date','subject','fromName','fromAddress','to','cc','replyTo','textPlain','textHtml'];

    public function emailAccount()
    {
    	return $this->hasOne("App\Models\Email",'id','accountId');
    }

    public function attachment()
    {
    	return $this->hasMany("App\Models\Attachments",'mailId','id');
    }
}
