<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachments extends Model
{
    public $table="attachments";
    public $fillable=['mailId','mailboxType','attachmentId','fileName','filePath'];

}
