<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Errorlogs extends Model
{
    public $table="errorlogs";
    public $fillable=['errormsg'];
}
