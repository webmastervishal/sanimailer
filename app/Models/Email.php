<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    public $table="email";
    public $fillable=['email','password','clientId','statusId'];

}
