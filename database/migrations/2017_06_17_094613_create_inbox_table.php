<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inbox', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accountId');
            $table->string('flag',50);
            $table->integer('mailId');
            $table->dateTime('date');
            $table->string('subject',500)->nullable();
            $table->string('fromName',100)->nullable();
            $table->string('fromAddress',100);
            $table->string('to',1000);
            $table->string('cc',1000)->nullable();
            $table->string('replyTo',1000);
            $table->longText('textPlain')->nullable();
            $table->longText('textHtml')->nullable();
            $table->string('status',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inbox');
    }
}
