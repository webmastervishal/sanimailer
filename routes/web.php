<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/get-inbox/{id}','MailBoxController@getInbox');
Route::get('/get-sent/{id}','MailBoxController@getSent');
Route::get('/get-spam/{id}','MailBoxController@getSpam');
Route::get('/get-junk/{id}','MailBoxController@getJunk');
Route::get('/get-drafts/{id}','MailBoxController@getDrafts');
Route::get('/get-trash/{id}','MailBoxController@getTrash');

Route::get('/get-inbox-mail/{id}','MailBoxController@getInboxMail');
Route::get('/get-sent-mail/{id}','MailBoxController@getSentMail');
Route::get('/get-spam-mail/{id}','MailBoxController@getSpamMail');
Route::get('/get-junk-mail/{id}','MailBoxController@getJunkMail');
Route::get('/get-drafts-mail/{id}','MailBoxController@getDraftsMail');
Route::get('/get-trash-mail/{id}','MailBoxController@getTrashMail');

Route::get('/get-inbox-count','MailBoxController@getInboxCount');
Route::get('/get-sent-count','MailBoxController@getSentCount');
Route::get('/get-spam-count','MailBoxController@getSpamCount');
Route::get('/get-junk-count','MailBoxController@getJunkCount');
Route::get('/get-drafts-count','MailBoxController@getDraftsCount');
Route::get('/get-trash-count','MailBoxController@getTrashCount');


Route::post('/set-inbox-status/{id}','MailBoxController@setInboxStatus');
Route::post('/set-sent-status/{id}','MailBoxController@setSentStatus');
Route::post('/set-spam-status/{id}','MailBoxController@setSpamStatus');
Route::post('/set-junk-status/{id}','MailBoxController@setJunkStatus');
Route::post('/set-drafts-status/{id}','MailBoxController@setDraftsStatus');
Route::post('/set-trash-status/{id}','MailBoxController@setTrashStatus');


Route::get('/delete-inbox-mail/{array}','MailBoxController@deleteInboxMail');
Route::get('/delete-sent-mail/{array}','MailBoxController@deleteSentMail');
Route::get('/delete-spam-mail/{array}','MailBoxController@deleteSpamMail');
Route::get('/delete-junk-mail/{array}','MailBoxController@deleteJunkMail');
Route::get('/delete-drafts-mail/{array}','MailBoxController@deleteDraftsMail');
Route::get('/delete-trash-mail/{array}','MailBoxController@deleteTrashMail');

